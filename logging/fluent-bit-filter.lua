function cb_print(tag, timestamp, record)
    sec = math.floor(timestamp)
    nsec = math.floor((timestamp - sec) * 10000)
    label = "{tag=\"" .. tag .. "\"}"
    entry = {}
    for i = 1,1 do
        entry[i] = {ts=os.date('%Y-%m-%dT%H:%M:%S',timestamp) .. '.' .. nsec .. 'Z', line=record["log"]}
    end
    streams = {streams={}}
    for i = 1,1 do
        streams["streams"][i] = {labels=label, entries=entry}
    end
    return 1, timestamp, streams
end
