import hug
import requests
from datetime import datetime

# [1573336139.000000000,
# {"pri"=>"30", "time"=>"Nov  9 21:48:59", "ident"=>"osuv",
# "message"=>"Starting System Logging Service..."}]
@hug.post('/loki')
def syslog(body=None):
    for item in body:
        if item.get("container_name"):
            tag = item.get("container_name").split(".")[0]
        else:
            tag = item.get("group")
        message = item.get('log')
        d = datetime.fromtimestamp(item.get('date'))
        ts = d.isoformat('T') + "Z"
        data = {'streams': [{'entries': [{'line': message, 'ts': ts }], 'labels': '{tag="' + tag + '"}'}]}
        r = requests.post('http://loki:3100/api/prom/push', json=data)


