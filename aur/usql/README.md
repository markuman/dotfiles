install `pacman -S go extra/unixodbc core/pkgconf`  

install from aur
  * oracle-instantclient-basic
  * oracle-instantclient-sdk


use this `oci8.pc`

```
libdir=/usr/lib/client64/lib
includedir=/usr/include

glib_genmarshal=glib-genmarshal
gobject_query=gobject-query
glib_mkenums=glib-mkenums

Name: oci8
Description: oci8 library
Libs: -L${libdir} -lclntsh
Cflags: -I${includedir}
Version: 18.3
```

then `export PKG_CONFIG_PATH=~/`

do ` go get -u -tags all github.com/xo/usql`

or ` go get -u -tags "all no_avatica no_couchbase" github.com/xo/usql`

and add `~/go/bin` to `$PATH`

done. 
