#
# ~/.bashrc
#

export PREFIX=$HOME/.$(whoami)/
## git
######
#source ~/.bash-git-prompt/gitprompt.sh
alias gs="git status"
alias gl="git log --all --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"
alias gp="git pull"
function lastweek() {
	git shortlog -sne --since="$(date --date='7 days ago')" --before="$(date)"
}
function gc() {
	if (npm run lint); then
		git commit -m "$1"
	fi
}

## ssh keygen
#############
alias newpubkey='ssh-keygen -f ~/.ssh/id_rsa -y'
alias fingerprint='ssh-keygen -lf ~/.ssh/id_rsa.pub'

## lazy
#######
alias mp3="mplayer *.mp3"
alias n="nano"
alias nani="nano"
alias zram="modprobe zram && echo $((3072*1024*1024)) > /sys/block/zram0/disksize && mkswap /dev/zram0 && swapon -p 60 /dev/zram0"

# mplayer vaapi
alias m="mplayer -vo vaapi -va vaapi"

function h () {
	grep "$1" ~/.bash_history
}

## lua
######
function lua_environment () {
	if (("$#" == 1)); then
		if [ $1 == "51" ]; then
			eval $(luarocks-5.1 path)
		elif [ $1 == "52" ]; then
			eval $(luarocks-5.2 path)
		elif [ $1 == "53" ]; then
			eval $(luarocks-5.3 path)
		fi
	fi
}
alias uselua="lua_environment"

export LUA_CPATH=$HOME/.$(whoami)/lib/lua/5.3/?.so

## matlab/octave
################
# alias matlab="/home/markus/MATLAB/bin/matlab"
# alias m="/home/markus/MATLAB/bin/matlab -nodesktop -nosplash"
alias o="octave-cli"


## local compiles
#################
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$HOME/.$(whoami)/lib"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.$(whoami)/bin" ] ; then
	export PATH="$HOME/.$(whoami)/bin:$HOME/.$(whoami)/usr/bin:$HOME/.$(whoami)/usr/local/bin:$PATH"
fi
 
if [ -d "$HOME/.$(whoami)/lib/pkgconfig" ]; then
	export PKG_CONFIG_PATH="$HOME/.$(whoami)/lib/pkgconfig"
fi

if [ $(uname) = "NetBSD" ]; then
	PKG_PATH="http://ftp.netbsd.org/pub/pkgsrc/packages/NetBSD/x86_64/7.0/All/"
	export PKG_PATH
fi

### autojump
# https://github.com/wting/autojump
if [ -f "/usr/share/autojump/autojump.bash" ]; then
	source /usr/share/autojump/autojump.bash
fi

## wttr.in
function wttr () {
        curl wttr.in/"$1"
}
alias wetter="wttr"

# gnumeric with english numeric notation
alias gnumeric='LC_NUMERIC="en" gnumeric'

# local webserver
alias ws="python -m http.server 8000"

export EDITOR=nano

## netstat without netstat
# https://staaldraad.github.io/2017/12/20/netstat-without-netstat/
function _netstat() {
awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2)); 
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
} 
NR > 1 {{if(NR==2)print "Local - Remote";local=getIP($2);remote=getIP($3)}{print local" - "remote}}' /proc/net/tcp 
}
alias ns=_netstat

function _docker_tabula_rasa(){
	docker rm $(docker ps -a -q) --force
	docker rmi $(docker images -q) --force
	yes|docker system prune
	docker volume rm $(docker volume ls -q --filter dangling=true) --force
}

alias updatebash="source ~/.bashrc"

function _rip_audio() {

  if (("$#" != 1)); then
    echo "source is missing..."
  else
    youtube-dl --prefer-ffmpeg --embed-thumbnail --add-metadata --metadata-from-title "%(artist)s - %(title)s" --audio-quality 0 --audio-format mp3 --extract-audio "$1"
  fi

}

function set_enc_clipboard() {
    xclip -out -selection clipboard | redis-cli -h 10.0.0.59 -x SET A
}

function get_enc_clipboard() {
    redis-cli -h 10.0.0.59 GET A | xclip -selection c
}

function o() {
	docker run --rm -ti --network dev --name octave -v $(pwd):~/ registry.gitlab.com/markuman/dev/octave:4.2.0 bash
}

function aur() {
	if (("$#" != 1)); then
		echo "source is missing..."
	else
		cd ~/aur
		git clone https://aur.archlinux.org/"$1".git
		cd $1
		makepkg
	fi
}

function awsenv() {

	if (("$#" != 1)); then
		cmp --silent ~/.aws/credentials ~/.aws/credentials.prod && echo "your're using PROD" || echo "you're using TEST"
	elif [ "$1" = "prod" ]; then
		cp ~/.aws/credentials.prod ~/.aws/credentials
	elif [ "$1" = "test" ]; then
		cp ~/.aws/credentials.test ~/.aws/credentials
	fi

}

alias x11orwayland="loginctl show-session $(loginctl | grep $(whoami) | awk '{print $1}') -p Type"
alias hosts="grep 'Host ' ~/.ssh/config"
alias fail="tail -f"

function _export(){
    if [ -z "$XONSH_ENCODING" ]
    then
        # this is not xonsh
        command export $@
    else
        # this is xonsh
        bash -c ! export $@
    fi
}

alias export=_export
alias ap=ansible-playbook
alias passgen='od -N 4 -t uL -An /dev/urandom | tr -d " "+%s | sha256sum | base64 | head -c 32 ; echo'

function redis-ping() {
        # ping a redis server at any cost
        redis-cli -h $1 ping 2>/dev/null || \
                echo $((printf "PING\r\n";) | nc $1 6379 2>/dev/null || \
                exec 3<>/dev/tcp/$1/6379 && echo -e "PING\r\n" >&3 && head -c 7 <&3)
}

