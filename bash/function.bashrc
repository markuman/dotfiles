function h () {
        grep "$1" ~/.bash_history
}

function wttr () {
	if [[ $# -eq 0 ]] ; then
		curl wttr.in/Berlin?format=4
        else
		curl wttr.in/"$1"
	fi
}

## netstat without netstat
# https://staaldraad.github.io/2017/12/20/netstat-without-netstat/
function _netstat() {
awk 'function hextodec(str,ret,n,i,k,c){
    ret = 0
    n = length(str)
    for (i = 1; i <= n; i++) {
        c = tolower(substr(str, i, 1))
        k = index("123456789abcdef", c)
        ret = ret * 16 + k
    }
    return ret
}
function getIP(str,ret){
    ret=hextodec(substr(str,index(str,":")-2,2));
    for (i=5; i>0; i-=2) {
        ret = ret"."hextodec(substr(str,i,2))
    }
    ret = ret":"hextodec(substr(str,index(str,":")+1,4))
    return ret
}
NR > 1 {{if(NR==2)print "Local - Remote";local=getIP($2);remote=getIP($3)}{print local" - "remote}}' /proc/net/tcp
}

function _rip_audio() {

  if (("$#" != 1)); then
    echo "source is missing..."
  else
    youtube-dl --prefer-ffmpeg --embed-thumbnail --add-metadata --metadata-from-title "%(artist)s - %(title)s" --audio-quality 0 --audio-format mp3 --extract-audio "$1"
  fi

}

function aur() {
        if (("$#" != 1)); then
                echo "source is missing..."
        else
                cd ~/aur
                git clone https://aur.archlinux.org/"$1".git
                cd $1
                makepkg
        fi
}

function redis-ping() {
        # ping a redis server at any cost
        redis-cli -h $1 ping 2>/dev/null || \
                echo $((printf "PING\r\n";) | nc $1 6379 2>/dev/null || \
                exec 3<>/dev/tcp/$1/6379 && echo -e "PING\r\n" >&3 && head -c 7 <&3)
}


# delete instead of rm -rf 
del() { 
	# mv to trash
	mkdir -p ~/.Trash
	mv "$@" ~/.Trash/
	# delete files older than 5 days
	find ~/.Trash -type f -mtime +5 -delete 
	# delete empty directories
	find ~/.Trash -type d -empty -delete
}

totp() {
        TOKEN=$(jq ".$1" -r totp.json)
        oathtool -b --totp $TOKEN
}