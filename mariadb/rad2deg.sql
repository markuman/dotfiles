CREATE OR REPLACE FUNCTION `rad2deg`(
		rad DOUBLE
	) RETURNS double
RETURN rad * 180.0 / pi();