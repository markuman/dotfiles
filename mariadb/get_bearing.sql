CREATE OR REPLACE FUNCTION `get_bearing`(
	lon1 DOUBLE,
	lat1 DOUBLE,
	lon2 DOUBLE,
	lat2 DOUBLE
) RETURNS double
BEGIN
	SET @rlat1 = deg2rad(lat1);
	SET @rlat2 = deg2rad(lat2);
	SET @rlon1 = deg2rad(lon1);
	SET @rlon2 = deg2rad(lon2);
	SET @dlon = deg2rad(lon2 - lon1);
	SET @b = atan2(sin(@dlon) * cos(@rlat2), cos(@rlat1) * sin(@rlat2) - sin(@rlat1) * cos(@rlat2) * cos(@dlon)); 
	SET @bd = rad2deg(@b);
	RETURN MOD(@bd + 360, 360);
END;