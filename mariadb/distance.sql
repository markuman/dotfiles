CREATE OR REPLACE FUNCTION `distance`(
	lon1 DOUBLE,
	lat1 DOUBLE,
	lon2 DOUBLE,
	lat2 DOUBLE
) RETURNS double
BEGIN
	SET @earth_radius = 6371;
	SET @dLat = deg2rad(lat2-lat1);
	SET @dLon = deg2rad(lon2-lon1);
	SET lat1 = deg2rad(lat1);
    SET lat2 = deg2rad(lat2);
	SET @a = SIN(@dLat/2) * SIN(@dLat/2) + SIN(@dLon/2) * SIN(@dLon/2) * COS(lat1) * COS(lat2);
	SET @c = 2 * ATAN2(SQRT(@a), SQRT(1-@a));
	RETURN @earth_radius * @c;
END;