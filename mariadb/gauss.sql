CREATE OR REPLACE FUNCTION `gauss`(
		x DOUBLE,
		c DOUBLE,
		sig DOUBLE
	) RETURNS double
RETURN EXP(
		(
			- 1 * POWER(( x - c ), 2 )/(
				2 * POWER( sig, 2 )
			)
		)
	);