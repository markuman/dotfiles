CREATE OR REPLACE FUNCTION `deg2rad`(
		deg DOUBLE
	) RETURNS double
RETURN deg * pi()/ 180;