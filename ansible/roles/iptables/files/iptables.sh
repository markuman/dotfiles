#!/bin/bash
sudo /sbin/iptables -I DOCKER-USER -p tcp --dport 24224 -j DROP
sudo /sbin/iptables -I DOCKER-USER -p tcp --dport 3307 -j DROP
sudo /sbin/iptables -I DOCKER-USER -p tcp --dport 2377 -j DROP