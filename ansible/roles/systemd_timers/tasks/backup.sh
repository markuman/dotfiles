#!/bin/bash

echo "mount rw"
sudo mount -o remount,rw /mnt/backup/
echo "dump nextcloud"
mysqldump --single-transaction nextcloud | zstd > /home/m/nextcloud.sql.zst
echo "dump gitea"
mysqldump --single-transaction gitea | zstd > /home/m/gitea.sql.zst
echo "rsnapshot"
sudo rsnapshot daily
echo "mount ro"
sudo mount -o remount,ro /mnt/backup/

