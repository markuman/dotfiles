# Gluster FS

- https://docs.gluster.org/en/latest/Administrator%20Guide/Performance%20Testing/
- https://github.com/distributed-system-analysis/smallfile
- `sudo python smallfile_cli.py --operation create --threads 2 --file-size 1024 --files 200 --top /mnt/vol1/eintollertest2/`

## write remote

```
total threads = 2
total files = 400
total IOPS = 400
total data = 0.391 GiB
100.00% of requested files processed, minimum is 70.00
elapsed time = 3.177
files/sec = 125.907879
IOPS = 125.907879
MiB/sec = 125.907879
```

## write local

... no surprise

```
total threads = 2
total files = 400
total IOPS = 400
total data = 0.391 GiB
100.00% of requested files processed, minimum is 70.00
elapsed time = 1.222
files/sec = 327.351881
IOPS = 327.351881
MiB/sec = 327.351881
```
