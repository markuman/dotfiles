import requests
import json
import os
import dateutil.parser
from datetime import datetime, timezone

token = os.environ['SCW_SECRET']
organization = os.environ['SCW_ORGANIZATION']

header = {'x-auth-token': token,'Content-type': 'application/json'}
snapshoturl = "https://cp-ams1.scaleway.com/snapshots"
serverurl = "https://cp-ams1.scaleway.com/servers"

r = requests.get(serverurl, headers=header)

# determine volume id of my server
volume_id = r.json()['servers'][0]['volumes']['0']['id']

# get all snapshots (because I will delete old ones)
r = requests.get(snapshoturl, headers=header)

for snapshot in r.json()['snapshots']:
    current_date = datetime.now(timezone.utc)
    snapshot_date = dateutil.parser.parse(snapshot['creation_date'])
    if (current_date - snapshot_date).days > 1:
        print("delete " + snapshot['name'])
        requests.delete(snapshoturl + '/' + snapshot['id'], headers=header)

# create a new snapshot
body = {'name': 'scw_snapshot_' + datetime.now().strftime("%Y-%m-%d"), 'organization': organization, 'volume_id': volume_id}
print("create snapshot: " + 'scw_snapshot_' + datetime.now().strftime("%Y-%m-%d"))
r = requests.post(snapshoturl, headers=header, data=json.dumps(body))
