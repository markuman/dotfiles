# aws-es-auth-proxy

based on python 2 service: https://github.com/lucaagostini/aws-es-auth-proxy

# usage

```
docker run -d --name aws-es-auth-proxy \
-p 8080:8080 \
-e AWS_ACCESS_KEY=... \
-e AWS_SECRET_KEY=... \
-e AWS_REGION=eu-central-1 \
-e AWS_ES_ENDPOINT=yourendpointurl.eu-central-1.es.amazonaws.com \
markuman/aws-es-auth-proxy
```