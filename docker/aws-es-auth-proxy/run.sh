#!/bin/sh

echo "[default]" > ~/.aws/credentials
echo "aws_access_key_id="$AWS_ACCESS_KEY >> ~/.aws/credentials
echo "aws_secret_access_key="$AWS_SECRET_KEY >> ~/.aws/credentials

aws-es-auth-proxy \
--proxy_host 0.0.0.0 \
--aws_service_region "$AWS_REGION" \
--aws_service_endpoint "$AWS_ES_ENDPOINT"