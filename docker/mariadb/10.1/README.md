**DRAFT**


# MariaDB Galera Cluster for Docker Swarm Mode

**1. create at least one network where all MariaDB Galera nodes can access each other**

`docker network create -d overlay --attachable dbcore`

**2. init your cluster**

the cluster will have the name `mdb`. the init node will get the service name `mdb_init`

`docker service create --name mdb_init --network dbcore -p3309:3306 mdb:10.1 ./start.sh init mdb`

check the cluster size

`docker exec -ti $(docker ps --format '{{.Names}}'|grep mdb_init) mysql -h 127.1 -e "show status like 'wsrep_cluster_size'"`

add a user

```
docker exec -ti $(docker ps --format '{{.Names}}'|grep mdb_init) mysql -h 127.1 -e "create user 'm'@'%' identified by 'nomysql1';"
docker exec -ti $(docker ps --format '{{.Names}}'|grep mdb_init) mysql -h 127.1 -e "grant all privileges on *.* to 'm'@'%' with grant option;"
``` 

No you should be able to connect with your client to 127.1 on port 3309.


**3. add nodes to the cluster**

`docker service create --name mdb --network dbcore -p 3310:3306 mdb:10.1 ./start.sh node mdb`

try to connect to your cluster (dbeaver, mycli, usql, mysql, mysql-workbench ...) on port 3010.  
when it works, scale up to two

`docker service scale mdb=2`

when the 2nds starts successfully, destroy the init seed `docker service scale mdb_init=0`  
and scale mdb up to three.


# limitations:

max scale size step = 1 (dunno why).
