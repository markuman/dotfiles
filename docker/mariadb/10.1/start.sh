#!/bin/bash

if (("$#" == 2)); then

    # adding new nodes to the galera cluster
    # first we try if the nodes in the swarm are reachable
    # if not, there are no nodes yet and we assume the there is only
    # a init seed (new galera cluster
    if [[ "$1" = "node" ]]; then
        sed -i 's/wsrep_cluster_address/#&/' /etc/mysql/my.cnf
        sed -i 's/wsrep_node_address/#&/' /etc/mysql/my.cnf
        sed -i "s/my_wsrep_cluster/$2/" /etc/mysql/my.cnf
	CLUSTERNAME="$2"_init

	ping -c 3 $CLUSTERNAME > /dev/null 2>&1
        # 0 means reachable
        if [ $? -ne 0 ]
        then
            CLUSTERNAME="$2"
        fi
	echo "using " $CLUSTERNAME
        mysqld_safe --wsrep_cluster_address=gcomm://"$CLUSTERNAME"
        tail -f /dev/null

    elif [[ "$1" = "init" ]]; then
        eth0=$(ip a |grep eth2 -A2 | grep "inet"|awk '{ print $2 }')
        ip=${eth0::-3}
        sed -i "s/172.18.0.2/$ip/g" /etc/mysql/my.cnf
        sed -i "s/my_wsrep_cluster/$2/" /etc/mysql/my.cnf
        # starting new cluster
        echo "starting new cluster " $2
        mysqld_safe --wsrep-new-cluster
    fi
fi

