Build with `docker build -t mariadb:alpine-10.3.13 .`

Start with `docker run -d -p 3306:3306 --name mariadb mariadb:alpine-10.3.13

Or with persistence shared folder with host: `docker run -ti --name mariadb -v $(pwd)/test:/var/lib/mysql/ -p 3306:3306 mariadb:alpine-10.3.13`

Init with

```
docker exec -ti mariadb mysql -h 127.1 -e "create user 'm'@'%' identified by 'nomysql1';"
docker exec -ti mariadb mysql -h 127.1 -e "grant all privileges on *.* to 'm'@'%' with grant option;"
```

# option

to enable slow_query_log, use `-e SLOW_LOG=true`.  
Log location is `/var/lib/mysql/slow_query.log`.

# upgrade from mariadb 10.2

If upgrade from mariadb 10.2, run

```
docker exec -ti mariadb mysql_upgrade
```
