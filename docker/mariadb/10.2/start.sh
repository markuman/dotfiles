#!/bin/sh

N=$(ls -l /var/lib/mysql|head -n 1|awk '{print $2}')

if [ $N -eq 0 ]
then
	echo "init mariadb"
	mysql_install_db --user=mysql --datadir=/var/lib/mysql
fi
echo "start mariadb"

if [ -z ${SLOW_LOG+x} ]; then
    mysqld_safe
else
    mysqld_safe --slow-query-log --slow-query-log-file=/var/lib/mysql/slow_query.log
fi
