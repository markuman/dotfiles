#!/bin/sh

N=$(ls -l /var/lib/mysql|head -n 1|awk '{print $2}')

if [ $N -eq 0 ]
then
	echo "init mariadb"
	mysql_install_db --user=mysql --datadir=/var/lib/mysql
fi

sed -i 's,skip-networking,#skip-networking,' /etc/my.cnf.d/mariadb-server.cnf
sed -i 's,#bind-address,bind-address,' /etc/my.cnf.d/mariadb-server.cnf

if [ -z ${SERVER_ID+x} ]; then
    SID=1
else
    SID="$SERVER_ID"
fi

if [ -z ${QUERY_CACHE+x} ]; then
    QCACHE=OFF
else
    QCACHE="$QUERY_CACHE"
fi

if [ -z ${INNODB_BUFFER_POOL} ]; then
    IBP=256M
else
    IBP="$INNODB_BUFFER_POOL"
fi

echo "given server id $SERVER_ID"
echo "using server id $SID"
echo "query cache is $QCACHE"

echo "start mariadb"

if [ -z ${SLOW_LOG+x} ]; then
    mysqld_safe --gtid-domain-id "$SID" --server-id "$SID" --innodb-buffer-pool-size="$IBP" --log-bin --query_cache_size 10M --query_cache_type "$QCACHE" ${READ_ONLY}
else
    mysqld_safe --gtid-domain-id "$SID" --server-id "$SID" --log-bin --query_cache_size 10M --query_cache_type "$QCACHE" --slow-query-log --slow-query-log-file=/var/lib/mysql/slow_query.log ${READ_ONLY}
fi
