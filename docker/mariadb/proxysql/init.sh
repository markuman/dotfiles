#!/bin/bash

nohup proxysql --idle-threads -f > proxysql.log 2>&1 &
ansible-playbook init.yml
tail -n 100 -f proxysql.log