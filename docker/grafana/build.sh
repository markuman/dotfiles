#!/bin/sh
apk update
apk add git nodejs npm
cd /mnt
git clone https://github.com/pR0Ps/grafana-trackmap-panel
cd grafana-trackmap-panel
npm install
npx grunt
