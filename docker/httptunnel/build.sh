#!/bin/sh
apk update 
apk add alpine-sdk autoconf automake git
cd /mnt
rm -rf httptunnel
git clone https://github.com/larsbrinkhoff/httptunnel
cd httptunnel
./autogen.sh
./configure
make
