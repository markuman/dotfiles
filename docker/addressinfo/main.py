import hug
from dns import resolver
import random
import time

@hug.get('/getip', examples='host=google.de&dns=8.8.8.8')
def echo(host: hug.types.text, dns: hug.types.text=None):
    res = resolver.Resolver()
    if dns is None:
        dns = res.nameservers
    else:
        res.nameservers = [dns]
    answers = res.query(host)
    retval = {host: [], 'dns': dns}
    for rdata in answers:
        retval[host].append(rdata.address)
    return retval

@hug.get('/ip', examples='/')
def get_session_data(request):
    return {'user-agent': request.user_agent, 'remote-addr': request.remote_addr }

@hug.get('/password', examples="len=8&delay=True")
def password(len: int=8, delay=None):
    if delay:
        time.sleep(random.randint(1,5))
    return {"password": ''.join(random.choice("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") for _ in range(len))}

