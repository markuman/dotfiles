# Database 

* [Redis](https://redis.io/) - KISS key/value in-memory database

## client stuff

* [DBeaver](http://dbeaver.jkiss.org/) - universal database client
* [MyCLI](https://github.com/dbcli/mycli) - A Terminal Client for MariaDB with AutoCompletion and Syntax Highlighting
* [usql](https://github.com/xo/usql) -  usql is a universal command-line interface for SQL databases
* [sqltabs](https://github.com/sasha-alias/sqltabs) - Rich SQL client for Postgresql, MySQL, MS SQL, Amazon Redshift 
* [remmina](https://gitlab.com/Remmina/Remmina/) - Remote Desktop Client (SSH, RDP ...)

# Editors

* [Geany](https://www.geany.org/) - GTK Editor and lua-scriptable
* [nano](https://www.nano-editor.org/) 
* [vs code](https://github.com/Microsoft/vscode) - vs code (even when it's microsoft, it is an awseome oss editor)

# Graphics

* [Blender](https://www.blender.org/) 
* [GIMP](https://www.gimp.org/downloads/) 
* [Inkscape](https://inkscape.org/en/) - powerful vector graphics
* [Kdenlive](https://kdenlive.org/) - video editor

# Utils

* [ncdu](https://dev.yorhel.nl/ncdu) NCurses Disk Usage
* [screen](https://www.gnu.org/software/screen/) terminal multiplexer and serial console 
* [youtube-dl](https://github.com/rg3/youtube-dl) download everything
* [axel](https://github.com/axel-download-accelerator/axel) light and fast command line download accelerator
* [autojump](https://github.com/wting/autojump) a `cd` command that leans
* [tilix](https://gnunn1.github.io/tilix-web/) GTK tiling terminal emulator
* [koala](https://github.com/oklai/koala/) GUI application for Less, Sass, Compass and CoffeeScript compilation
* [peek](https://github.com/phw/peek) screen capture

# iptables

* [shorewall](http://shorewall.org/)

# sonstiges

* [astral](https://astralapp.com/) organize your github stars
* [xlunch](https://github.com/Tomas-M/xlunch) minimalistischer desktop launcher für rpi
* [DavMail](http://davmail.sourceforge.net/) - in case your company provide only exchange and not imap

# self-hosted

* [gitea](https://github.com/go-gitea/gitea) gitea

# No FOSS :(

* [typora](https://typora.io/) minimal md editor

